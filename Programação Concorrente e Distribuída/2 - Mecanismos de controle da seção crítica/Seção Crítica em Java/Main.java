public class Main {
  public static void main(String[] a) {
    final CarWindow win = new CarWindow();

    win.pack();
    win.setVisible(true);

    new Thread(new Runnable() {
      public void run() {
        while (true) {
          win.repaint();
        }
      }
    }).start();
  }
}
