import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TrafficController {

  private Lock trafficLock;

  public TrafficController(Lock mutex) {
    this.trafficLock = mutex;
  }

  public void enterLeft() {
    this.trafficLock.lock();
  }

  public void enterRight() {
    this.trafficLock.lock();
  }

  public void leaveLeft() {
    this.trafficLock.unlock();
  }

  public void leaveRight() {
    this.trafficLock.unlock();
  }

}
