#include <cstdlib>
#include <chrono>
#include <iostream>
#include <pthread.h>

const auto BOARD_ROWS = 2048;
const auto BOARD_COLS = 2048;
const auto NUM_GENERATIONS = 2000;
const auto SRAND_VALUE = 1985;

bool* board;
bool* new_board;

/* #region Board Logic */

inline auto at(int i, int j) -> int {
  return i * BOARD_COLS + j;
}

auto number_of_neighbors(bool *board, int i, int j) -> int {
  auto acc = 0;

  for (auto x = i - 1; x <= i + 1; x++) {
    const auto x_index = x < 0 ? BOARD_ROWS - 1 : x == BOARD_ROWS ? 0 : x;

    for (auto y = j - 1; y <= j + 1; y++) {
      if (y == j && x == i) continue;

      const auto y_index = y < 0 ? BOARD_COLS - 1 : y == BOARD_COLS ? 0 : y;

      acc += board[at(x_index, y_index)];
    }
  }

  return acc;
}

void add_glider(bool *board, int row, int col) {
  board[at(row, col + 1)] = true;
  board[at(row + 1, col + 2)] = true;
  board[at(row + 1, col)] = true;
  board[at(row + 2, col + 1)] = true;
  board[at(row + 2, col + 2)] = true;
}

void add_r_pentomino(bool *board, int row, int col) {
  board[at(row, col + 1)] = true;
  board[at(row, col + 2)] = true;
  board[at(row + 1, col)] = true;
  board[at(row + 1, col + 1)] = true;
  board[at(row + 2, col + 1)] = true;
}

void populate_grid(bool *board) {
  add_glider(board, 1, 1);
  add_r_pentomino(board, 10, 30);
}

auto count_live_cells(bool *board) -> int {
  int acc = 0;

  #pragma omp parallel for reduction(+: acc)
  for (int i = 0; i < BOARD_ROWS; i++)
    for (int j = 0; j < BOARD_COLS; j++)
      acc += board[at(i, j)];

  return acc;
}

/* #endregion */

auto main() -> int{
  bool* temp_board;
  board = (bool*) malloc(BOARD_ROWS * BOARD_COLS * sizeof(bool));
  new_board = (bool*) malloc(BOARD_ROWS * BOARD_COLS * sizeof(bool));
  populate_grid(board);

  std::cout << "Live cells: " << count_live_cells(board) << std::endl;

  auto start = std::chrono::high_resolution_clock::now();

  for (auto g = 0; g < NUM_GENERATIONS; g++) {
    #pragma omp parallel for
    for (auto i = 0; i < BOARD_ROWS; i++) {
      for(auto j = 0; j < BOARD_COLS; j++) {
        auto neighbors = number_of_neighbors(board, i, j);
        auto index = at(i, j);

        if (
          (!board[index] && neighbors == 3) ||
          (board[index] && (neighbors == 2 || neighbors == 3))
        ) {
          new_board[index] = true;
        }
        else {
          new_board[index] = false;
        }
      }
    }

    temp_board = board;
    board = new_board;
    new_board = temp_board;
  }

  auto finish = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double> elapsed = finish - start;
  std::cout << "Elapsed time: " << elapsed.count() << " s\n";

  start = std::chrono::high_resolution_clock::now();
  auto live_cells = count_live_cells(board);
  finish = std::chrono::high_resolution_clock::now();

  elapsed = finish - start;
  std::cout << "Generation sum time: " << elapsed.count() << " s\n";
  std::cout << "Live cells: " << live_cells << std::endl;

  free(new_board);
  free(board);

  return 0;
}
