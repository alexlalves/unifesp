#include <cassert>
#include <cstdlib>
#include <iostream>
#include <pthread.h>
#include <unistd.h>

const auto THREADS = 4;

size_t request = 0;
size_t respond = 0;

int global_sum = 0;

void* client_thread(void* args) {
  auto thread_id = (size_t) args;

  while (true) {
    // while (respond != thread_id) {
    //   request = thread_id;
    //   pthread_yield();
    // }

    {
      int local_sum = global_sum;
      global_sum = local_sum + 1;

      if (thread_id == 1)
        pthread_yield();

      assert(local_sum == global_sum - 1);
      std::cout << thread_id << ": " << global_sum << std::endl;
    }

    // respond = 0;
  }
}

void* server_thread(void* args) {
  while (true) {
    // while (request == 0) pthread_yield();
    // respond = request;

    // while (respond != 0) pthread_yield();
    // request = 0;
  }
}

int main() {
  srand(1024);

  std::cout << "Hello" << std::endl;

  pthread_t threads[THREADS + 1];

  pthread_create(&threads[0], nullptr, server_thread, nullptr);

  for (size_t t = 1; t <= THREADS; t++) {
    pthread_create(&threads[t], nullptr, client_thread, (void*) t);
  }

  pthread_join(threads[0], nullptr);

  return 0;
}
