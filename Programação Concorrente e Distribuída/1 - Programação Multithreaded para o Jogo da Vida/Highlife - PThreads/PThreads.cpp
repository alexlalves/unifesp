#include <cstdlib>
#include <chrono>
#include <iostream>
#include <vector>
#include <array>
#include <pthread.h>

constexpr auto BOARD_ROWS = 2048;
constexpr auto BOARD_COLS = 2048;
constexpr auto NUM_GENERATIONS = 2000;
constexpr auto SRAND_VALUE = 1985;
constexpr auto THREADS = 16;
constexpr auto BOARD_SIZE = BOARD_ROWS * BOARD_COLS;

std::vector<bool> board(BOARD_SIZE, false);
std::vector<bool> new_board;

inline auto at(int i, int j) -> int {
  return i * BOARD_COLS + j;
}

auto number_of_neighbors(std::vector<bool> &board, int row, int col) -> int {
  auto acc = 0;

  for (auto x = row - 1; x <= row + 1; x++) {
    const auto x_index = x < 0 ? BOARD_ROWS - 1 : x == BOARD_ROWS ? 0 : x;

    for (auto y = col - 1; y <= col + 1; y++) {
      if (y == col && x == row) continue;

      const auto y_index = y < 0 ? BOARD_COLS - 1
        : y == BOARD_COLS ? 0
        : y;

      acc += board[at(x_index, y_index)];
    }
  }

  return acc;
}

void add_glider(std::vector<bool> &board, int row, int col) {
  board[at(row, col + 1)] = true;
  board[at(row + 1, col + 2)] = true;
  board[at(row + 1, col)] = true;
  board[at(row + 2, col + 1)] = true;
  board[at(row + 2, col + 2)] = true;
}

void add_r_pentomino(std::vector<bool> &board, int row, int col) {
  board[at(row, col + 1)] = true;
  board[at(row, col + 2)] = true;
  board[at(row + 1, col)] = true;
  board[at(row + 1, col + 1)] = true;
  board[at(row + 2, col + 1)] = true;
}

void populate_grid(std::vector<bool> &board) {
  add_glider(board, 1, 1);
  add_r_pentomino(board, 10, 30);
}

auto count_live_cells(std::vector<bool> &board) -> int {
  int acc = 0;

  for (size_t i = 0; i < BOARD_SIZE; i++)
    acc+= board[i] % 2;

  return acc;
}

inline auto is_cell_alive(bool cell_state, int neighbors) -> bool {
  return neighbors == 3
    || (cell_state && neighbors == 2)
    || (!cell_state && neighbors == 6);
}

auto compute_new_board(void *args) -> void* {
  auto thread_id = (size_t) args;

  auto start = (BOARD_ROWS / THREADS) * thread_id;
  auto end = start + BOARD_ROWS / THREADS;

  for (auto i = start; i < end; i++) {
    for(auto j = 0; j < BOARD_COLS; j++) {
      auto neighbors = number_of_neighbors(board, i, j);
      auto index = at(i, j);

      new_board[index] = is_cell_alive(board[index], neighbors);
    }
  }

  return nullptr;
}

auto main() -> int{
  new_board.resize(BOARD_SIZE);

  populate_grid(board);

  std::array<pthread_t, THREADS> threads;

  std::cout << "Live cells: " << count_live_cells(board) << std::endl;

  auto start = std::chrono::high_resolution_clock::now();

  for (auto g = 0; g < NUM_GENERATIONS; g++) {
    for (auto t = 0; t < THREADS; t++) {
      pthread_create(
        &threads[t],
        nullptr,
        compute_new_board,
        (void*) t
      );
    }

    for (auto t = 0; t < THREADS; t++) {
      pthread_join(threads[t], nullptr);
    }

    board.swap(new_board);

    // std::cout << "Live cells: " << count_live_cells(board) <<  std::endl;
  }

  auto finish = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double> elapsed = finish - start;
  std::cout << "Elapsed time: " << elapsed.count() << " s\n";

  std::cout << "Live cells: " << count_live_cells(board) << std::endl;

  return 0;
}
