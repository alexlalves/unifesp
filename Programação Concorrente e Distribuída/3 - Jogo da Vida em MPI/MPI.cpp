#include <cstdlib>
#include <chrono>
#include <iostream>
#include <mpi.h>

const auto BOARD_ROWS = 2048;
const auto BOARD_COLS = 2048;

const auto NUM_GENERATIONS = 2000;

int NUM_PROCESSES;
int DOMAIN_FIRST_ROW; // Inclusive
int DOMAIN_LAST_ROW; // Inclusive
int DOMAIN_ROW_COUNT;

int PROCESS_ID;

int BOARD_SIZE;

bool* local_board;
bool* new_local_board;

auto local_at(int row, int col) -> int {
  return (row + 1) * BOARD_COLS + col;
}

auto get_local_neighbors(bool *board, int i, int j) -> int {
  auto acc = 0;

  for (auto x = i - 1; x <= i + 1; x++) {
    for (auto y = j - 1; y <= j + 1; y++) {
      if (y == j && x == i) continue;
      const auto y_index = y < 0 ? BOARD_COLS - 1 : y == BOARD_COLS ? 0 : y;

      acc += board[local_at(x, y_index)];
    }
  }

  return acc;
}

auto row_in_domain(int row) -> int {
  return DOMAIN_FIRST_ROW <= row && row <= DOMAIN_LAST_ROW;
}

auto convert_to_local_row(int global_row) -> int {
  return global_row - DOMAIN_FIRST_ROW;
}

void add_local_glider(bool *board, int row, int col) {
  auto local_row = convert_to_local_row(row);

  if (row_in_domain(row)) {
    board[local_at(local_row, col + 1)] = true;
  }

  if (row_in_domain(row + 1)) {
    board[local_at(local_row + 1, col + 2)] = true;
    board[local_at(local_row + 1, col)] = true;
  }

  if (row_in_domain(row + 2)) {
    board[local_at(local_row + 2, col + 1)] = true;
    board[local_at(local_row + 2, col + 2)] = true;
  }
}

void add_local_r_pentomino(bool *board, int row, int col) {
  auto local_row = convert_to_local_row(row);

  if (row_in_domain(row)) {
    board[local_at(local_row, col + 1)] = true;
    board[local_at(local_row, col + 2)] = true;
  }

  if (row_in_domain(row + 1)) {
    board[local_at(local_row + 1, col)] = true;
    board[local_at(local_row + 1, col + 1)] = true;
  }

  if (row_in_domain(row + 2)) {
    board[local_at(local_row + 2, col + 1)] = true;
  }
}

void populate_local_grid (bool *board) {
  add_local_glider(board, 1, 1);
  add_local_r_pentomino(board, 10, 30);
}

auto allocate_new_local_matrix() -> bool* {
  return (bool*) calloc((DOMAIN_ROW_COUNT + 2) * BOARD_COLS, sizeof(bool));
}

auto count_local_live_cells(bool *board) -> int {
  int acc = 0;

  for (int i = 0; i < DOMAIN_ROW_COUNT; i++)
    for (int j = 0; j < BOARD_COLS; j++) {
      acc += board[local_at(i, j)];
    }

  return acc;
}

void compute_new_local_board() {
  for (auto i = 0; i < DOMAIN_ROW_COUNT; i++) {
    for(auto j = 0; j < BOARD_COLS; j++) {
      auto neighbors = get_local_neighbors(local_board, i, j);
      auto index = local_at(i, j);

      if (
        (!local_board[index] && neighbors == 3) ||
        (local_board[index] && (neighbors == 2 || neighbors == 3))
      ) {
        new_local_board[index] = true;
      }
      else {
        new_local_board[index] = false;
      }
    }
  }

  return;
}

auto main(int argc, char* argv[]) -> int {
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROCESSES);
  MPI_Comm_rank(MPI_COMM_WORLD, &PROCESS_ID);

  auto start = std::chrono::high_resolution_clock::now();

  auto ROWS_PER_PROCESS = BOARD_ROWS / NUM_PROCESSES;

  DOMAIN_FIRST_ROW = ROWS_PER_PROCESS * PROCESS_ID;
  DOMAIN_LAST_ROW = DOMAIN_FIRST_ROW + ROWS_PER_PROCESS - 1;
  DOMAIN_ROW_COUNT = (DOMAIN_LAST_ROW - DOMAIN_FIRST_ROW + 1);

  BOARD_SIZE = (DOMAIN_ROW_COUNT + 2) * BOARD_COLS;

  auto NEXT_PROCESS_ID = (PROCESS_ID + 1) % NUM_PROCESSES;
  auto PREVIOUS_PROCESS_ID = (NUM_PROCESSES + PROCESS_ID - 1) % NUM_PROCESSES;

  auto SEND_TOP_RECEIVE_BOTTOM_ROW_TAG = 1;
  auto SEND_BOTTOM_RECEIVE_TOP_ROW_TAG = 2;

  local_board = allocate_new_local_matrix();
  new_local_board = allocate_new_local_matrix();

  populate_local_grid(local_board);

  MPI_Request r1, r2, r3, r4;

  for (auto g = 0; g < NUM_GENERATIONS; g++) {
    MPI_Isend(
      //buffer
      local_board + BOARD_COLS, // FIRST ROW
      // count
      BOARD_COLS,
      // datatype
      MPI_C_BOOL,
      // dest
      PREVIOUS_PROCESS_ID,
      // tag
      SEND_TOP_RECEIVE_BOTTOM_ROW_TAG,
      // comm
      MPI_COMM_WORLD,
      // request
      &r1
    );

    MPI_Isend(
      //buffer
      local_board + DOMAIN_ROW_COUNT * BOARD_COLS, // LAST ROW
      // count
      BOARD_COLS,
      // datatype
      MPI_C_BOOL,
      // dest
      NEXT_PROCESS_ID,
      // tag
      SEND_BOTTOM_RECEIVE_TOP_ROW_TAG,
      // comm
      MPI_COMM_WORLD,
      // request
      &r2
    );

    MPI_Irecv(
      // buffer
      local_board + (1 + DOMAIN_ROW_COUNT) * BOARD_COLS,
      // count
      BOARD_COLS,
      // datatype
      MPI_C_BOOL,
      // source
      NEXT_PROCESS_ID,
      // tag
      SEND_TOP_RECEIVE_BOTTOM_ROW_TAG,
      // comm
      MPI_COMM_WORLD,
      // request
      &r3
    );

    MPI_Irecv(
      // buffer
      local_board + 0 * BOARD_COLS,
      // count
      BOARD_COLS,
      // datatype
      MPI_C_BOOL,
      // source
      PREVIOUS_PROCESS_ID,
      // tag
      SEND_BOTTOM_RECEIVE_TOP_ROW_TAG,
      // comm
      MPI_COMM_WORLD,
      // request
      &r4
    );

    MPI_Wait(&r3, MPI_STATUS_IGNORE);
    MPI_Wait(&r4, MPI_STATUS_IGNORE);

    compute_new_local_board();

    free(local_board);
    local_board = new_local_board;
    new_local_board = allocate_new_local_matrix();
  }

  auto local_live_cells = count_local_live_cells(local_board);
  auto total_global_cells = 0;

  MPI_Reduce(
    &local_live_cells,
    &total_global_cells,
    1,
    MPI_INT,
    MPI_SUM,
    0,
    MPI_COMM_WORLD
  );

  auto finish = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double> elapsed = finish - start;

  if (PROCESS_ID == 0) {
    std::cout << "Total global cells: " << total_global_cells << std::endl;
    std::cout << "Elapsed time: " << elapsed.count() << " s\n";
  }

  free(new_local_board);

  MPI_Finalize();

  return 0;
}
